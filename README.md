#Desafio Neoway Senior Data Scientist
### Author: Marcus Oliveira da Silva
### Janeiro de 2018

## Objetivo 1 
O primeiro objetivo do desafio foi completado no projeto 'pyspark-model'. Lá uso
um notebook para treinar/criar/exportar um modelo de predição de churn usando
a Spark ML lib.

* Link do MD: [Aqui](pyspark-model/spark-train-model.md)
* Link do Notebook: [Aqui](pyspark-model/spark-train-model.ipynb)

## Objetivo 2
O segundo objetivo do desafio foi completado no projeto 'scala-churnapi'. Lá
foi desenvolvida uma aplicação em scala para deployar modelos spark como um 
serviço REST. Usei o modelo do objetivo um para criar um endpoint de predição
de churn. As instruções para rodar a aplicação local estão no README do projeto.
Mas fiz o deploy da aplicação em um servidor web para facilitar a avaliação.

Link do servidor: [http://159.65.28.88:9000](http://159.65.28.88:9000)

Link do README da aplicação: [Aqui](scala-churnapi/README.md)

## Observações:
* O modelo treinado salvo (formato zip) fica no path 'pyspark-model/model/pyspark.rf.zip'
* O modelo treinado salvo (formato JSON) fica no path: 'pyspark-model/model/pyspark.rf/'
* O binário de execução da aplicação REST fica no path '/dist/churnapi-1.0.zip'
* O código da aplicação fica no path 'scala-churnapi/app'
* O código dos testes da aplicação fica no path 'scala-churnapi/test'
* Os passos para montar o ambiente de dev(usei o MacOS) estão no arquivo config_env.sh
