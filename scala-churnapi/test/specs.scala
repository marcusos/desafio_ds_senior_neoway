import entities.ChurnData
import org.specs2.mutable._
import services.ChurnModelService

import scalaz.Inject

object ChurnModelServiceSpec extends Specification {

  val service = new ChurnModelService

  "ChurnModelService" should {
    "Predict" in {
      " 0 (not churn)" in {
        var churnDataFalse = new ChurnData(
          "Brittany Martinez",
          39,
          13029.61,
          1,
          4.83,
          8,
          "2015-11-09 02:41:26",
          "4019 James Street Apt. 731 North Carmen, OK 01929",
          "Ryan, Mcdonald and Garcia"
        )

        var notChunrPrediction = service.fitPrediction(churnDataFalse)
        notChunrPrediction mustEqual 0.0
      }
      " 1 (churn)" in {
        var churnDataTrue = new ChurnData(
          "Kevin Mueller",
          41,
          11916.22,
          0,
          6.5,
          11,
          "2013-08-13 00:38:46",
          "6157 Frank Gardens Suite 019 Carloshaven, RI 17756",
          "Wilson PLC"
        )

        var churnPrediction = service.fitPrediction(churnDataTrue)
        churnPrediction mustEqual 1.0
      }
    }
  }
}
