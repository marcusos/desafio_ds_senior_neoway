# Churn Prediction REST API - Scala Application
### Author: Marcus Oliveira da SIlva

A REST application to predicts churn from incoming JSON data. 


## About
This project is a REST application to a deploy random forest model to predict churn from incoming data. It uses a Spark ML trained model, MLeap(to bundle,save and load a ml model),  and Scala Play framework (to build the REST API). MLeap allows data scientists and engineers to deploy machine learning pipelines from Spark to a portable format and execution engine.

## Application Structure
churnapi uses Scala 2.11 and SBT to manage project dependencies 

1. Controllers
    * HomeController (application's home page)
    * PredictController (handle churn predictions over JSON HTTP requests.)
2. Services
    * ChurnModelService (a singleton service that loads a spark random forest model to memory and output incoming churn predictions)
3. Entities
    * ChurnData (maps incomings JSON churn data to a POJO object) 
4. Models
    * pyspark.rf.zip (the random forest model exported from pyspark)    
5. Tests
    * ChurnModelServiceSpec (test if the expected predictions from ChurnModelService are corrects)

## Test the REST API on a server
I have deployed the application to a digital ocean server for test purpose. 
Send a POST request to http://159.65.28.88:9000/predict/churn with a ChurData JSON in the payload:
```bash
# Predicted as Not Churn (0)
curl -X POST \
  http://159.65.28.88:9000:9000/predict/churn \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
    "Name": "Brittany Martinez",
    "Age": 39,
    "Total_Purchase": 13029.61,
    "Account_Manager": 1,
    "Years": 4.83,
    "Num_Sites": 8,
    "Onboard_date": "2015-11-09 02:41:26",
    "Location": "4019 James Street Apt. 731 North Carmen, OK 01929",
    "Company": "Ryan, Mcdonald and Garcia"
  }'
  
# Predicted as Churn (1)
curl -X POST \
  http://159.65.28.88:9000:9000/predict/churn \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: e1b7fa76-00ff-3376-f920-56b61b031eae' \
  -d '{
    "Name": "Kevin Mueller",
    "Age": 41,
    "Total_Purchase": 11916.22,
    "Account_Manager": 0,
    "Years": 6.5,
    "Num_Sites": 11,
    "Onboard_date": "2013-08-13 00:38:46",
    "Location": "6157 Frank Gardens Suite 019 Carloshaven, RI 17756",
    "Company": "Wilson PLC"
  }
```

## Run the REST API local
To run the application, go to the dist folder in the root of this repository, unzip the file (churnapi-1.0.zip) and then run the script in the bin directory. It comes in two versions, a bash shell script, and a windows .bat script. 
For Unix users it will be required to set the script as an executable:
```bash
cd ../dist
# unzipe app
unzip churnapi-1.0.zip
# set as an executable
chmod +x churnapi-1.0/bin/churnapi
# run the server
churnapi-1.0/bin/churnapi -Dplay.http.secret.key=churnapi -Dplay.http.port=9000
```
Go to the localhost:9000 and check if the home screen is working

## Use the REST API in localhost
Send a POST request to localhost:9000/predict/churn with a ChurData JSON in the payload:
```bash
# Predicted as Not Churn (0)
curl -X POST \
  http://localhost:9000/predict/churn \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
    "Name": "Brittany Martinez",
    "Age": 39,
    "Total_Purchase": 13029.61,
    "Account_Manager": 1,
    "Years": 4.83,
    "Num_Sites": 8,
    "Onboard_date": "2015-11-09 02:41:26",
    "Location": "4019 James Street Apt. 731 North Carmen, OK 01929",
    "Company": "Ryan, Mcdonald and Garcia"
  }'
  
# Predicted as Churn (1)
curl -X POST \
  http://localhost:9000/predict/churn \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: e1b7fa76-00ff-3376-f920-56b61b031eae' \
  -d '{
    "Name": "Kevin Mueller",
    "Age": 41,
    "Total_Purchase": 11916.22,
    "Account_Manager": 0,
    "Years": 6.5,
    "Num_Sites": 11,
    "Onboard_date": "2013-08-13 00:38:46",
    "Location": "6157 Frank Gardens Suite 019 Carloshaven, RI 17756",
    "Company": "Wilson PLC"
  }
```

## References
* [Spark](https://spark.apache.org/docs/2.2.0/ml-guide.html)
* [MLeap](https://github.com/combust/mleap)
* [Play Framewor](https://www.playframework.com/)