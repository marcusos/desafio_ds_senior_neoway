package controllers

import javax.inject.{Inject, Singleton}

import entities.ChurnData
import play.api.mvc.{AbstractController, ControllerComponents}
import play.api.libs.json._
import services.ChurnModelService

/**
  * This controller handle churn predictions over JSON HTTP requests
  */
@Singleton
class PredictController @Inject()
  (cc: ControllerComponents,  churnModelService: ChurnModelService) extends AbstractController(cc) {

  /**
    * Create an Action to parse the json input churn data
    * Call the ChurnModelService to fit a prediction
    * and return churn = 0|1
    */
  def predictChurn = Action { implicit request =>

    // Parse input data
    implicit val residentReads = Json.reads[ChurnData]
    val json = request.body.asJson.get
    val churnData = json.as[ChurnData]

    // Call the Churn Model Service and Fit a Prediction
    val prediction = churnModelService.fitPrediction(churnData)

    // Return the prediction as JSON response
    val response: JsValue = Json.obj("churn" -> prediction)
    Ok(response)
  }

}
