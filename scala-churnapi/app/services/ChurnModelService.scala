package services

import java.io.File
import java.util.Date
import javax.inject.Inject
import ml.combust.mleap.runtime._
import ml.combust.mleap.core.types._
import ml.combust.bundle.BundleFile
import ml.combust.mleap.runtime.MleapSupport._
import entities.ChurnData
import play.Environment
import resource.managed

class ChurnModelService {

  // Loading the random forest spark model to memory
  println("ChurnModelService instantiated ( Loading pyspark.rf.zip to memory...)")

  val modelBundle = (for(bundleFile <- managed(BundleFile((new File("pyspark.rf.zip" ))))) yield {
    bundleFile.loadMleapBundle().get
  }).opt.get

  // Create the model input schema
  def getModelScheme(): StructType = {

    val schema: StructType = StructType(
    StructField("Age", ScalarType.Double),
    StructField("Total_Purchase", ScalarType.Double),
    StructField("Account_Manager", ScalarType.Double),
    StructField("Years", ScalarType.Double),
    StructField("Num_sites", ScalarType.Double),
    StructField("Onboard_date_diff", ScalarType.Double)
    ).get

    return schema
  }

  // Parse str to date in the following format: yyyy-MM-dd hh:mm:ss
  def parseDate(dateStr: String): Date = {
    val format = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
    return format.parse(dateStr)
  }

  // Diff in days from two dates
  def dateDiff(newerDate: Date, olderDate: Date): Long = {
    val diffInDays = ( (newerDate.getTime() - olderDate.getTime()) / (1000 * 60 * 60 * 24) )
    return diffInDays
  }

  // Function to fit a churn prediction on a churn data
  def fitPrediction(churnData: ChurnData): Double = {

    // Extracting the Onboard_date_diff featture
    val Onboard_date_diff = this.dateDiff(
      this.parseDate("2017-01-01 23:59:59"), this.parseDate(churnData.Onboard_date)
    ).toDouble

    // Building the model input dataset with the correct features
    val dataset: LocalDataset = LocalDataset(
      Row(
        churnData.Age,
        churnData.Total_Purchase,
        churnData.Account_Manager.toDouble,
        churnData.Years,
        churnData.Num_Sites,
        Onboard_date_diff
      )
    )

    // Build a leap DataFrame
    val leapFrame = LeapFrame(this.getModelScheme(), dataset)

    // Get the root pipeline
    val mleapPipeline = modelBundle.root

    // Transform our leap frame using the pipeline
    val predictions = (for(lf <- mleapPipeline.transform(leapFrame);
                           lf2 <- lf.select("prediction")) yield {
      lf2.dataset.map(_.getDouble(0))
    }).get.toSeq

    // Get the first prediction
    return predictions.head
  }
}
