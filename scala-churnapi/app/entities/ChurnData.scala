package entities

/**
  * Pojo class to map the JSON churn data entity
  */
case class ChurnData(Name: String, Age: Double, Total_Purchase: Double, Account_Manager: Int, Years: Double, Num_Sites: Double,
             Onboard_date: String, Location: String, Company: String)
