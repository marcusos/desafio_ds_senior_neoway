# Install python 3.6 with anaconda
bash Miniconda3-latest-MacOSX-x86_64.sh

# Add following code to the .zshrc
export PATH="/Users/marcus/anaconda3/bin:$PATH"

# Install spark
brew install apache-spark

# Install pyspark
pip install pyspark

# Add following code to the .zshrc to use jupyter notebook as the pyspark driver
export PYSPARK_DRIVER_PYTHON=jupyter
export PYSPARK_DRIVER_PYTHON_OPTS='notebook'

# Install mleap
pip install mleap

# Install Scala
brew install scala

# Install sbt
brew install sbt

# Run the spark shell with mleap support
# This commend will open a notebook with PySpark and Mleap libs
spark-shell --packages ml.combust.mleap:mleap-spark_2.11:0.8.1