
# Neoway Machine Learning Challenge
## Create/Train a Churn prediction model using Spark ML
### Author: Marcus Oliveira da Silva

On this nootebook I will use  Apache Spark ML to create/train a machine learning model that will help classify whether or not a customer will churn. And will export this model using MLeap to be used in a churn prediction REST application

The available data about customers have the following fields:
* Name : Name of the latest contact at Company
* Age: Customer Age
* Total_Purchase: Total Ads Purchased
* Account_Manager: Binary 0=No manager, 1= Account manager assigned
* Years: Totaly Years as a customer
* Num_sites: Number of websites that use the service.
* Onboard_date: Date that the name of the latest contact was onboarded
* Location: Client HQ Address
* Company: Name of Client Company
* Churn: Churn Indicator (1= Positive churn, 0= Negative hurn)

## 1 - Read Data
Let's first load all the csv churn data and print some rows.


```python
from pyspark.sql import SQLContext
from pyspark.sql.types import *
from pyspark.sql.functions import regexp_extract, regexp_replace

sqlContext = SQLContext(sc)
schema = StructType([ \
    StructField("Name", StringType(), True), \
    StructField("Age", DoubleType(), True), \
    StructField("Total_Purchase", DoubleType(), True), \
    StructField("Account_Manager", DoubleType(), True), \
    StructField("Years", DoubleType(), True), \
    StructField("Num_sites", DoubleType(), True), \
    StructField("Onboard_date", StringType(), True), \
    StructField("Location", StringType(), True), \
    StructField("Company", StringType(), True), \
    StructField("Churn", DoubleType(), True)])

churn_data = sqlContext.read \
    .format('com.databricks.spark.csv') \
    .option("header", "true") \
    .load('data/customer_churn.csv', schema = schema)

churn_data.show(n=2)
```

    +----------------+----+--------------+---------------+-----+---------+-------------------+--------------------+----------+-----+
    |            Name| Age|Total_Purchase|Account_Manager|Years|Num_sites|       Onboard_date|            Location|   Company|Churn|
    +----------------+----+--------------+---------------+-----+---------+-------------------+--------------------+----------+-----+
    |Cameron Williams|42.0|       11066.8|            0.0| 7.22|      8.0|2013-08-30 07:00:40|10265 Elizabeth M...|Harvey LLC|  1.0|
    |   Kevin Mueller|41.0|      11916.22|            0.0|  6.5|     11.0|2013-08-13 00:38:46|6157 Frank Garden...|Wilson PLC|  1.0|
    +----------------+----+--------------+---------------+-----+---------+-------------------+--------------------+----------+-----+
    only showing top 2 rows
    


# 2 - Feature Extraction
There is some columns in string and datetime format like the Onboard_date column, next I will transform the Onboard_date to a diff in days from a future date 


```python
from pyspark.sql.functions import to_timestamp
from pyspark.sql.functions import datediff, to_date, lit

# Regex to get address ([A-Z]{2})

# Get a numerical feature from the Onboard_date
churn_data = churn_data.withColumn("Onboard_date_diff", datediff(to_date(lit("2017-01-01")),
                       to_date("Onboard_date","yyyy-MM-dd")))

# Rename Churn to label
churn_data = churn_data.withColumnRenamed("Churn", "label")


churn_data.select("Age", "Total_Purchase", "Account_Manager", "Years", "Num_sites", 
                  "Onboard_date_diff", "label").show(n=10, truncate= True)
```

    +----+--------------+---------------+-----+---------+-----------------+-----+
    | Age|Total_Purchase|Account_Manager|Years|Num_sites|Onboard_date_diff|label|
    +----+--------------+---------------+-----+---------+-----------------+-----+
    |42.0|       11066.8|            0.0| 7.22|      8.0|             1220|  1.0|
    |41.0|      11916.22|            0.0|  6.5|     11.0|             1237|  1.0|
    |38.0|      12884.75|            0.0| 6.67|     12.0|              186|  1.0|
    |42.0|       8010.76|            0.0| 6.71|     10.0|              985|  1.0|
    |37.0|       9191.58|            0.0| 5.56|      9.0|              348|  1.0|
    |48.0|      10356.02|            0.0| 5.12|      8.0|             2861|  1.0|
    |44.0|      11331.58|            1.0| 5.23|     11.0|               27|  1.0|
    |32.0|       9885.12|            1.0| 6.92|      9.0|             3951|  1.0|
    |43.0|       14062.6|            1.0| 5.46|     11.0|             1921|  1.0|
    |40.0|       8066.94|            1.0| 7.11|     11.0|             3932|  1.0|
    +----+--------------+---------------+-----+---------+-----------------+-----+
    only showing top 10 rows
    


Now I will extract the state from the 'Location' address column using regex


```python
from pyspark.sql.functions import col, when

#Regex pattern to fit the State from address
pattern = ", ([A-Z]{2})" 

#Apply the regex function to the Location column
churn_data = churn_data.withColumn("State", regexp_extract('Location', pattern, 1))
#churn_data = churn_data.withColumn("State", regexp_replace('State', '', 'NOPATTERN'))

churn_data = churn_data.withColumn("State",
    when(col('State') != '', col('State')).otherwise(None))
churn_data = churn_data.na.fill({'State': 'NOPATTERN'})

#Print the new extracted feature
churn_data.groupBy("State").count().show(n=10)
```

    +---------+-----+
    |    State|count|
    +---------+-----+
    |       AZ|   14|
    |       SC|   18|
    |       LA|   17|
    |       MN|   17|
    |       NJ|   13|
    |       DC|   15|
    |NOPATTERN|  107|
    |       OR|   10|
    |       VA|   16|
    |       RI|   14|
    +---------+-----+
    only showing top 10 rows
    


# 3 - Feature Selection and Model Training


Next I will:

* Gather all features I need into a single column in the DataFrame.
* Split labeled data into training and testing set
* Build a grid search to find the best hyperparameters
* Fit the model to the training data.

## Feature Assembler
Its necessary to select the input features. For this problem I will pick the: "Age", "Total_Purchase", "Account_Manager", "Years", "Num_sites", "Onboard_date_diff" columns and try to fit a model using these columns. The model perfomed worse with the extracted "State" feature, than I decided to remove "State" from the input


```python
from pyspark.ml.feature import StringIndexer
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.feature import OneHotEncoder

# Tryed to use the OneHotEnconder on the State future but the model wa worse
# stringIndexer = StringIndexer(inputCol="State", outputCol="StateIndex")
# encoder = OneHotEncoder(dropLast=False, inputCol="StateIndex", outputCol="StateVec")

# Vector Assembler to set all features I need into a single column
selected_cols = [
    "Age", "Total_Purchase", "Account_Manager", "Years", "Num_sites", "Onboard_date_diff"
]
assembler = VectorAssembler(inputCols = selected_cols, outputCol = 'features')

```

## Model Training - Random Forest
I can now define the classifier and the pipeline. With this done, I can split our labeled data in train and test sets and fit a model. I will use grid search to search over hyperparameters and select the best model

Resons to use random forest:

- I chosed to start simple to get a sense of how hard the problem is
- Random forest reduce variance (overfitting) compared to decisions trees
- The input data has few features
- Random forest  is easy to interpret and is good to get features importance


```python
from pyspark.ml import Pipeline
from pyspark.ml.classification import RandomForestClassifier
from pyspark.ml.tuning import ParamGridBuilder, TrainValidationSplit, CrossValidator
from pyspark.ml.evaluation import BinaryClassificationEvaluator, MulticlassClassificationEvaluator

# Picking random forest as the classifier model 
classifier = RandomForestClassifier(labelCol = 'label', featuresCol = 'features', seed=42) 

# Configure an ML pipeline, which consists of two stages: assembler, classifier
pipeline = Pipeline(stages=[stringIndexer, assembler, classifier])

# ParamGridBuilder constructs a grid of parameters to search over.
paramGrid = ParamGridBuilder() \
    .addGrid(classifier.numTrees, [5, 15, 50]) \
    .addGrid(classifier.maxDepth, [5, 10]) \
    .build()
    
# TrainValidationSplit will try all combinations of values and determine best model using
# the evaluator.
# If the input had more data I would try CrossValidator insted of TrainValidationSplit
tvs = TrainValidationSplit(estimator=pipeline,
                           estimatorParamMaps=paramGrid,
                           evaluator=BinaryClassificationEvaluator(),
                           # 80% of the data will be used for training, 20% for validation.
                           trainRatio=0.8)

# Split the data into training and test sets (20% held out for testing)
(train, test) = churn_data.randomSplit([0.80, 0.20], seed = 42)

# Run the grid search, and choose the best set of parameters.
tvsModel = tvs.fit(churn_data)
```

Cheking the hyperparameters(numTrees, maxDepth) of the best found:


```python
print('Metrics: ', tvsModel.validationMetrics)
print('numTrees: ', paramGrid[4][classifier.numTrees])
print('maxDepth: ', paramGrid[4][classifier.maxDepth])
```

    Metrics:  [0.8180821917808219, 0.7908219178082191, 0.8334246575342465, 0.8131506849315068, 0.8413698630136982, 0.8321917808219179]
    numTrees:  50
    maxDepth:  5


The **best hyperparameters** setting found by the grid search are: **numTrees=50 and maxDepth=5**

## Model Evaluation
The most important question to ask:

Is my predictor better than random guessing?

How do we quantify that?

Measure the area under the ROC curve.

Reference: http://gim.unmc.edu/dxtests/roc3.htm


```python
# Fit a model using the best hyperparameters from the previus steps
rf = RandomForestClassifier(labelCol = 'label', featuresCol = 'features', maxDepth=5, numTrees=50, seed=42) 
pipeline = Pipeline(stages=[stringIndexer, encoder, assembler, rf])
model = pipeline.fit(train)

# Get predictions
predictions = model.transform(test)

# Compute Binary Classification Metrics (AUROC and AUPR)
evaluator = BinaryClassificationEvaluator()
auroc = evaluator.evaluate(predictions, {evaluator.metricName: "areaUnderROC"})
aupr = evaluator.evaluate(predictions, {evaluator.metricName: "areaUnderPR"})
print("The AUPR is %s" % (aupr))
print("The AUROC is %s" % (auroc))

#  Other Metrics**
lp = predictions.select( "label", "prediction")
tp = lp[(lp.label == 1) & (lp.prediction == 1)].count()
tn = lp[(lp.label == 0) & (lp.prediction == 0)].count()
fp = lp[(lp.label == 0) & (lp.prediction == 1)].count()
fn = lp[(lp.label == 1) & (lp.prediction == 0)].count()

print("True Positives:", tp)
print("True Negatives:", tn)
print("False Positives:", fp)
print("False Negatives:", fn)
print("Total", lp.count())

r = float(tp)/(tp + fn)
print ("recall (tp/tp+fn): ", r)

p = float(tp) / (tp + fp)
print ("precision (tp/tp+fp):", p)

f1 = 2 * (p*r)/(p+r)
print ("f1:", f1)

accuracy = (tp+tn)/(tp+fp+tn+fn)
print ("accuracy:", accuracy)
```

    The AUPR is 0.8482823590548217
    The AUROC is 0.9603914259086674
    True Positives: 16
    True Negatives: 144
    False Positives: 4
    False Negatives: 13
    Total 177
    recall (tp/tp+fn):  0.5517241379310345
    precision (tp/tp+fp): 0.8
    f1: 0.6530612244897959
    accuracy: 0.903954802259887


Even the model looks like having a good performance (good AUROC and accuracy), the recall is low, It is happening because the input dataset has unbalanced data, there is a lote more 0.0 (not churn cases) than 1.0 (is churn cases). Applying some resample technics would improve the recall metric.


```python
# There is 150 is churn samples
print("is churn: ", churn_data.filter("label==1").count())
# There is 750
print("not churn: ", churn_data.filter("label==0").count())
```

    is churn:  150
    not churn:  750


## Model Evaluation with Down-sampling
For train data: I randomly assigned an int as ‘randIndex’ to each majority data point, and then filter out those whose ‘randIndex’ is larger than a threshold, so that the data points from the majority class 0.0 (not chunr) will be much less.


```python
from numpy.random import randint
from pyspark.sql.functions import udf
from pyspark.sql.types import IntegerType

## ratio of 0.0 to 1.0 in the trainSubsample
RATIO_ADJUST = 2.0 
 
counts = train.select('label').groupBy('label').count().collect()
higherBound = counts[0][1]
TRESHOLD_TO_FILTER = int(RATIO_ADJUST * float(counts[1][1]) / counts[0][1] * higherBound)
 
randGen = lambda x: randint(0, higherBound) if x == 0 else -1
 
udfRandGen = udf(randGen, IntegerType())
train = train.withColumn("randIndex", udfRandGen("label"))
trainSubsample = train.filter(train['randIndex'] < TRESHOLD_TO_FILTER)
trainSubsample = trainSubsample.drop('randIndex')
 
print("Distribution of Pos and Neg cases of the down-sampled training data are: \n", trainSubsample.groupBy("label").count().take(3))
```

    Distribution of Pos and Neg cases of the down-sampled training data are: 
     [Row(label=0.0, count=239), Row(label=1.0, count=121)]


Measure the metrics again


```python
# Fit a model using the best hyperparameters from the previus steps
model = pipeline.fit(trainSubsample)

# Get predictions
predictions = model.transform(test)

# Compute Binary Classification Metrics (AUROC and AUPR)
evaluator = BinaryClassificationEvaluator()
auroc = evaluator.evaluate(predictions, {evaluator.metricName: "areaUnderROC"})
aupr = evaluator.evaluate(predictions, {evaluator.metricName: "areaUnderPR"})
print("The AUPR is %s" % (aupr))
print("The AUROC is %s" % (auroc))

#  Other Metrics**
lp = predictions.select( "label", "prediction")
tp = lp[(lp.label == 1) & (lp.prediction == 1)].count()
tn = lp[(lp.label == 0) & (lp.prediction == 0)].count()
fp = lp[(lp.label == 0) & (lp.prediction == 1)].count()
fn = lp[(lp.label == 1) & (lp.prediction == 0)].count()

print("True Positives:", tp)
print("True Negatives:", tn)
print("False Positives:", fp)
print("False Negatives:", fn)
print("Total", lp.count())

r = float(tp)/(tp + fn)
print ("recall (tp/tp+fn): ", r)

p = float(tp) / (tp + fp)
print ("precision (tp/tp+fp):", p)

f1 = 2 * (p*r)/(p+r)
print ("f1:", f1)

accuracy = (tp+tn)/(tp+fp+tn+fn)
print ("accuracy:", accuracy)
```

    The AUPR is 0.819028767414264
    The AUROC is 0.938490214352284
    True Positives: 23
    True Negatives: 139
    False Positives: 9
    False Negatives: 6
    Total 177
    recall (tp/tp+fn):  0.7931034482758621
    precision (tp/tp+fp): 0.71875
    f1: 0.7540983606557378
    accuracy: 0.9152542372881356


Its looks like a far **better model**, the precision and recall balance metric **F1 mesure (0.75 vs 0.65)** is much better now for churn prediction, there is more true positives.

### Features Importance

Generally, importance provides a score that indicates how useful or valuable each feature was in the construction of the decision trees within the model. We can check from the code bellow the 'Years' and 'Num_sites' has 71% of overall features importance


```python
rfModel = model.stages[-1]
featureImportances = rfModel.featureImportances

# Estimate of the importance of each feature. (Normalize importances for tree to sum to 1.)
for i in range(0, len(selected_cols)):
    print("[%s] : %s \n" % (featureImportances[i], selected_cols[i]))
```

    [0.0907794110263] : Age 
    
    [0.0798877386899] : Total_Purchase 
    
    [0.00865224957977] : Account_Manager 
    
    [0.187731275924] : Years 
    
    [0.550051087405] : Num_sites 
    
    [0.0828982373751] : Onboard_date_diff 
    


### Using Mleap to export the model

Spark pipelines are not meant to be run outside of Spark. They require a DataFrame and therefore a SparkContext to run. These are expensive data structures and libraries to include in a project. With MLeap, there is no dependency on Spark to execute a pipeline. MLeap dependencies are lightweight and we use fast data structures to execute your ML pipelines. I will use the exported mleap model to deploy a churn prediction REST application


```python
import mleap.pyspark
from mleap.pyspark.spark_support import SimpleSparkSerializer

# Export the model as a mleap bundle (change the path to valid local path) 
model.serializeToBundle( 
    "jar:file:/Users/marcus/Projetos/desafios/desafio_ds_senior_neoway/pyspark-model/model/pyspark.rf.zip", model.transform(train))
```
